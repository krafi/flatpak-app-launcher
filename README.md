# Flatpak Application Launcher

This Python script helps you list and launch Flatpak applications on your Fedora system. It creates a graphical user interface using Tkinter, providing an easy way to access your installed Flatpak apps.

## Prerequisites

Before using this script, make sure you have the following installed on your system:

- Python3
- Tkinter
- Flatpak

## Usage

1. Clone or download this repository to your local machine.

2. Run the script:

    ```bash
    python3 flatpak_app_launcher.py
    ```

3. The script will list your Flatpak applications and create buttons for each one. Click a button to launch the respective application.


If you find that Flatpak doesn't create .desktop files for your applications on your system, it might be an issue specific to your setup or Flatpak integration. You can manually create .desktop files for your applications in the `/usr/share/applications/` directory to help resolve this.

## License

This project is licensed under the GNU General Public License v2.0. You can find the full license text in the [LICENSE](LICENSE) file.

## Disclaimer

This script was created for personal use and may not cover all possible use cases or scenarios. Use it at your own discretion.

## Author

Kazi ar rafi

## Contributing

If you want to contribute to this project or have suggestions, feel free to open an issue or submit a pull request.

