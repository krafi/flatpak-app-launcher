import subprocess
import re

def list_flatpak_apps():
    output = subprocess.check_output(['flatpak', 'list'], text=True)

    #pattern = r'([a-z]+\.[a-z]+\.[A-Za-z0-9.]+)\s'
    pattern = r'([a-zA-Z]+\.[a-zA-Z]+\.[A-Za-z0-9.-]+)\s'

    matches = re.findall(pattern, output)

    flatpak_info = []
    for match in matches:
        #print(match)

        name_pattern = re.compile(f'([^\\n]+){match}')
        name_match = name_pattern.search(output)
        if name_match:
            name = name_match.group(1).strip().replace(' ', '_')
        else:
            name = "Name not found"

        print(f'{name} -> {match}')

        flatpak_info.append({"name": name, "Application_ID": match})

    return flatpak_info


flatpak_info = list_flatpak_apps()
flatpak_info.sort(key=lambda x: x["name"])



output_file = "flatpak_info.json"

#import sys
#import json
#with open(output_file, 'w') as json_file:
#    json.dump(flatpak_info, json_file, indent=2)
#print(f'Data saved to {output_file}')


def run_flatpak(app_id):
    try:
        window.withdraw()
        subprocess.run(["flatpak", "run", app_id], check=True)

    except subprocess.CalledProcessError:
        window.withdraw()
        print(f"Failed to run {app_id}")

#with open("flatpak_info.json", "r") as file:
#    flatpak_info = json.load(file)


import tkinter as tk
from tkinter import ttk

bg_color = "#284e74"
text_color = "#f7f7f7"
#hover_bg_color = "white"
#hover_text_color = "black"
hover_bg_color = "#44475a"
hover_text_color = "#ff79c6"
window = tk.Tk()
window.title("Flatpak App Launcher")


window.configure(bg="#488bd2")
style = ttk.Style()
style.configure("TButton",
                background=bg_color,
                foreground=text_color,
                padding=(10, 5),
                margin=(10, 5))
style.map("TButton",
          background=[("active", hover_bg_color)],
          foreground=[("active", hover_text_color)])

row = 0
col = 0
for app_data in flatpak_info:
    name = app_data["name"]
    app_id = app_data["Application_ID"]

    button = ttk.Button(window, text=name, command=lambda app_id=app_id: run_flatpak(app_id))
    button.configure(style="TButton", padding=(10, 5))
    button.grid(row=row, column=col, padx=10, pady=5)

    col += 1
    if col >= 5:
        col = 0
        row += 1

window.mainloop()
